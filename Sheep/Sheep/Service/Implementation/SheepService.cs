﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Sheep.Models;
using Sheep.Service.Interface;

namespace Sheep.Service.Implementation
{
    public class SheepService : ISheepService
    {
        public XmlDocument DeleteShepherd(string xml)
        {
            var responseModel = new deleteShepherdResponse
            {
                shepherd = new deleteShepherdResponseShepherd()
            };
            var stringWriter = new StringWriter();
            var writer = XmlWriter.Create(stringWriter);
            try
            {
                var serializer = new XmlSerializer(typeof(deleteShepherdRequest));
                deleteShepherdRequest model;
                using (var s = GenerateStreamFromString(xml))
                {
                    model = (deleteShepherdRequest)serializer.Deserialize(s);
                }

                Shepherd shepherd;
                using (var context = new SheepEntities())
                {
                    shepherd = context.Shepherd.First(x => x.Id == model.shepherd.shepherdId);
                    shepherd.IsDeleted = true;
                    context.SaveChanges();
                }

                responseModel = new deleteShepherdResponse
                {
                    shepherd = new deleteShepherdResponseShepherd
                    {
                        deletedShepherdId = shepherd.Id,
                        deletedShepherdIdSpecified = shepherd.IsDeleted
                    }
                };

            }
            catch (Exception ex)
            {
                responseModel.shepherd.errors = new[] { new ct_errorsError { code = 1, codeSpecified = true, Value = ex.Message }, };
            }
            var responseSerializer = new XmlSerializer(typeof(deleteShepherdResponse));
            responseSerializer.Serialize(writer, responseModel);

            var doc = new XmlDocument();
            doc.LoadXml(stringWriter.ToString());
            return doc;
        }

        public XmlDocument ListAllShepherd(string xml)
        {
            var responseModel = new getAllShepherdsResponse
            {
                shepherds = new[] { new getAllShepherdsResponseShepherd(), }
            };
            var stringWriter = new StringWriter();
            var writer = XmlWriter.Create(stringWriter);
            try
            {
                var serializer = new XmlSerializer(typeof(getAllShepherdsRequest));
                getAllShepherdsRequest model;
                using (var s = GenerateStreamFromString(xml))
                {
                    model = (getAllShepherdsRequest)serializer.Deserialize(s);
                }

                IEnumerable<Shepherd> shepherds;
                using (var context = new SheepEntities())
                {
                    shepherds = context.Shepherd
                        .Include(x => x.Sheep)
                        .ToList();
                }

                responseModel.shepherds =
                    shepherds.Where(x => !x.IsDeleted)
                    .Select(x => new getAllShepherdsResponseShepherd
                    {
                        name = x.Name,
                        sheeps = model.listSheeps ? x.Sheep.Select(y => new getAllShepherdsResponseShepherdSheep
                        {
                            colour = y.Colour,
                            createdOn = y.CreatedOn,
                            id = x.Id
                        }).ToArray() : null,
                        shepherdId = x.Id,
                        shepherdIdSpecified = true
                    }).ToArray();



            }
            catch (Exception ex)
            {
                //takie errory bo skad brac inne?
                foreach (var item in responseModel.shepherds)
                {
                    item.errors = new[] { new ct_errorsError { code = 1, codeSpecified = true, Value = ex.Message }, };
                }
            }

            var responseSerializer = new XmlSerializer(typeof(getAllShepherdsResponse));
            responseSerializer.Serialize(writer, responseModel);

            var doc = new XmlDocument();
            doc.LoadXml(stringWriter.ToString());
            return doc;
        }

        public XmlDocument CreateShepherdAndSheep(string xml)
        {
            var responseModel = new createShepherdResponse()
            {
                shepherd = new createShepherdResponseShepherd()
            };
            var stringWriter = new StringWriter();
            var writer = XmlWriter.Create(stringWriter);
            try
            {
                var serializer = new XmlSerializer(typeof(createShepherdRequest));
                createShepherdRequest model;
                using (var s = GenerateStreamFromString(xml))
                {
                    model = (createShepherdRequest)serializer.Deserialize(s);
                }

                using (var context = new SheepEntities())
                {
                    var entity = context.Shepherd.Add(
                        new Shepherd
                        {
                            IsDeleted = false,
                            Name = model.shepherd.name,
                            Sheep = model.shepherd.sheeps.Select(x => new Models.Sheep
                            {
                                Colour = x.colour
                            }).ToList()
                        });
                    context.SaveChanges();


                    responseModel.shepherd.shepherdId = entity.Id;
                    responseModel.shepherd.shepherdIdSpecified = true;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var msg in from eve in e.EntityValidationErrors
                                    let msg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
eve.Entry.Entity.GetType().Name, eve.Entry.State)
                                    select eve.ValidationErrors.Aggregate(msg, (current, ve) => current + string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage)))
                {
                    responseModel.shepherd.errors = new[] { new ct_errorsError { code = 1, codeSpecified = true, Value = msg }, };
                }
            }
            catch (Exception ex)
            {
                responseModel.shepherd.errors = new[] { new ct_errorsError { code = 1, codeSpecified = true, Value = ex.Message }, };
            }

            var responseSerializer = new XmlSerializer(typeof(createShepherdResponse));
            responseSerializer.Serialize(writer, responseModel);

            var doc = new XmlDocument();
            doc.LoadXml(stringWriter.ToString());
            return doc;
        }

        private Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}