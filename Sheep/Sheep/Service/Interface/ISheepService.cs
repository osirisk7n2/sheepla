﻿using System.Xml;

namespace Sheep.Service.Interface
{
    public interface ISheepService
    {
        XmlDocument DeleteShepherd(string xml);

        XmlDocument ListAllShepherd(string xml);

        XmlDocument CreateShepherdAndSheep(string xml);
    }
}