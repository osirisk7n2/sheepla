﻿using System;
using System.Web.Mvc;
using Sheep.ViewModels.Sheep;
using Sheep.ViewService.Interface;

namespace Sheep.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISheepViewService _sheepViewService;

        public HomeController(
            ISheepViewService sheepViewService)
        {
            _sheepViewService = sheepViewService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new SheepPageViewModel();
            return View(model);
        }

        [HttpPost, ActionName("Index")]
        public ActionResult IndexPost()
        {
            var model = new SheepPageViewModel();
            if (!TryUpdateModel(model) || !ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var doc = _sheepViewService.FulfillTask(model);
                model.Response = doc.OuterXml;
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, e.Message);
                return View(model);
            }

            return View(model);
        }

    }
}
