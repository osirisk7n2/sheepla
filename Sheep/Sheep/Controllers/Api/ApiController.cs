﻿using System;
using System.Web.Mvc;
using Sheep.ViewModels.Sheep;
using Sheep.ViewService.Interface;

namespace Sheep.Controllers.Api
{
    public class ApiController : Controller
    {
        private readonly ISheepViewService _sheepViewService;

        public ApiController(
            ISheepViewService sheepViewService)
        {
            _sheepViewService = sheepViewService;
        }

        public ActionResult SendRequest()
        {
            var model = new SheepPageViewModel();
            if (!TryUpdateModel(model) || !ModelState.IsValid)
            {
                //zwracać czysty xml czy opakowany?, łatwiej tak bo potem prościej błędy ogarnąć
                //return Content(model.Response, "text/xml");
                return Json(new { Success = false });
            }

            try
            {
                var doc = _sheepViewService.FulfillTask(model);
                model.Response = doc.OuterXml;
            }
            catch (Exception e)
            {
                //return Content(model.Response, "text/xml");
                return Json(new { Success = false });
            }
            //var errors = ModelState.Values.Select(x=>x.Errors);
            //return Content(model.Response, "text/xml");
            return Json(new { Success = true, Data = model.Response });
        }

    }
}
