﻿using System.Xml;
using Sheep.Enums;
using Sheep.Service.Interface;
using Sheep.ViewService.Interface;

namespace Sheep.ViewService.Implementation
{
    public class SheepViewService : ISheepViewService
    {
        private readonly ISheepService _sheepService;

        public SheepViewService(
            ISheepService sheepService)
        {
            _sheepService = sheepService;
        }

        public XmlDocument FulfillTask(ViewModels.Sheep.SheepPageViewModel model)
        {
            XmlDocument doc;

            switch (model.OperationChoosen)
            {
                case OperationChoosenEnum.CreateShepherdWithSheep:
                    {
                        doc = _sheepService.CreateShepherdAndSheep(model.XmlString);
                        break;
                    }
                case OperationChoosenEnum.DeleteShepherd:
                    {
                        doc = _sheepService.DeleteShepherd(model.XmlString);
                        break;
                    }
                case OperationChoosenEnum.ListAllShepherds:
                    {
                        doc = _sheepService.ListAllShepherd(model.XmlString);
                        break;
                    }
                default:
                    {
                        doc = new XmlDocument();
                        break;
                    }
            }

            return doc;
        }
    }
}