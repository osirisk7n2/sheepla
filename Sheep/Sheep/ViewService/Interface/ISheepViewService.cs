﻿using System.Xml;
using Sheep.ViewModels.Sheep;

namespace Sheep.ViewService.Interface
{
    public interface ISheepViewService
    {
        XmlDocument FulfillTask(SheepPageViewModel model);
    }
}