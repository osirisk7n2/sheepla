﻿namespace Sheep.Enums
{
    public enum OperationChoosenEnum
    {
        CreateShepherdWithSheep = 1,
        ListAllShepherds = 2,
        DeleteShepherd = 3
    }
}