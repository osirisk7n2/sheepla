﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Sheep.Enums;

namespace Sheep.ViewModels.Sheep
{
    public class SheepPageViewModel : IValidatableObject
    {
        [DisplayName("Xml"), Required(ErrorMessage = "To pole jest wymagane"), AllowHtml]
        public string XmlString { get; set; }

        [DisplayName("AJAX")]
        public bool SendAjax { get; set; }

        public OperationChoosenEnum OperationChoosen { get; set; }

        [DisplayName("Odpowiedź"), AllowHtml]
        public string Response { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var validationResults = new List<ValidationResult>();
            try
            {
                var file = HttpContext.Current.Server.MapPath("/XMLSchema/Sheep.xsd");

                var schemas = new XmlSchemaSet();
                var fileStream = new FileStream(file, FileMode.Open);
                schemas.Add(null, XmlReader.Create(fileStream));
                fileStream.Dispose();

                var doc = XDocument.Parse(XmlString);
                var msg = "";
                doc.Validate(schemas, (o, e) =>
                {
                    msg += e.Message + Environment.NewLine;
                });
                if (!string.IsNullOrWhiteSpace(msg))
                {
                    validationResults.Add(new ValidationResult("Coś się nie zgadza w przesłanym XMLu.... "));
                    validationResults.Add(new ValidationResult(msg));
                }
            }
            catch (Exception e)
            {
                validationResults.Add(new ValidationResult("Coś się nie zgadza w przesłanym XMLu.... "));
                validationResults.Add(new ValidationResult(e.Message));
            }
            return validationResults;
        }

    }
}